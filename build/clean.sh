SCRIPTPATH=$(readlink -f "$0")
CURRDIR=$(dirname "$SCRIPTPATH")
cd $CURRDIR

rm -r CMakeFiles
rm -r run.dir
rm cmake_install.cmake
rm CMakeCache.txt
rm Makefile
rm libuwimg++.so
rm ../run
