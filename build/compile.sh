SCRIPTPATH=$(readlink -f "$0")
echo $SCRIPTPATH
CURRDIR=$(dirname "$SCRIPTPATH")

cd $CURRDIR

cmake .
make
