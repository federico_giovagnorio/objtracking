#define BNBINS 8
#define GNBINS 8
#define RNBINS 8

#define EMPTY_HISTOGRAM     -1
#define COLOR_HISTOGRAM      1
#define GRACL_HISTOGRAM      2

#define FOREGROUND_HISTOGRAM 1
#define BACKGROUND_HISTOGRAM 2

#define MAG_TRESHOLD         20

using namespace std;
using namespace cv;


class Histogram {

public:

    int type;

    int bBins;
    int gBins;
    int rBins;
    
    float data[BNBINS][GNBINS][RNBINS];

    //costruttore
    Histogram();

    //costruttore
    Histogram(Mat frame, Rect window, int histogramType, int gnd);

    void buildData(Mat frame, Rect window, int histogramType , int gnd);

    //decostruttore
    ~Histogram();

    //ritorna true se istogramma vuoto
    bool empty();

    //rende istogramma una pdf
    void normalize();

    //ritorna il valore minimo non zero di un istogramma
    float minNonZero();

    //stampa un istogramma
    void print();
};

//calcola il peso del pixel nell'istogramma in base alla distanza dal centro della finestra
float foregroundKernel(float valueForKernel);

//calcola il peso del pixel nell'istogramma in base alla distanza dal centro della finestra
float backgroundKernel(float valueForKernel);

//costruisce ampiezza gradiente per ogni canale della zona di interesse
vector<Mat> getMagnitude(Mat frame, Rect window);

//calcola coefficiente Bhattacharyya tra due istogrammi
float bhattaCoeff(Histogram& hist1, Histogram& hist2);

//trasforma un istogramma usando le informazioni di background
void histByBackground(Histogram& foreHist, Histogram& backHist);

//calcola pdf del target
Histogram buildTargetPdf(Mat& frame, Rect& window, int histogramType);

//calcola pdf del target usando informazioni del background
Histogram buildTargetPdfByBackground(Mat& frame, Rect& window, int histogramType);