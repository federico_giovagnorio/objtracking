#include <stdio.h>
#include <cmath>
#include <bits/stdc++.h>
#include <opencv2/opencv.hpp>
#include <filesystem>

#include "histogram.h"
#include "tracking.h"
#include "wma.h"
#include "utils.h"

void run(VideoCapture &cap, String capName, int histogramType, int live, 
                                    int useAngle, int useAvg, int test128) {

    unsigned int frameNum = 1;
    int toWait = TIME_TO_WAIT;
    if (live)
        toWait = 5;

    float bhattaC;
    unsigned int meanIt;
    Scalar windowColor = Scalar(255, 255, 255);
    Scalar expColor = Scalar(0, 50, 255);
    
    //crea directory e file di output
    std::filesystem::remove_all(DATA_PATH + capName + "OUT");
    std::filesystem::create_directory(DATA_PATH + capName + "OUT");
    int save = 0;
    ofstream results;
    results.open(DATA_PATH + capName + "OUT/results.txt", ofstream::out | ofstream::trunc);
    
    //frame e finestra
    Mat frame;
    Rect window;
    
    // per test temple color 128, finestra attesa, file di input e output
    Rect expWindow;
    ifstream expPos;
    ofstream results128;   
    
    //legge primo frame dalla cartella di riferimento del test e crea la finestra del modello nel primo frame
    if (test128){
        expPos.open(DATA_PATH + capName + "/" + capName + "_gt.txt");
        window128(expPos, expWindow);
        window = Rect(expWindow);
        window.height -= (window.height + 1) %2;
        window.width -=  (window.width + 1) %2;

        frame = imread(DATA_PATH + capName + "/img/0001.jpg");
        if (frame.empty()){
            cout << "[!] ERRORE APERTURA FRAME 1" << endl;
            return;
        }
        
        if (save){
            Mat frame1 = frame.clone();
            draw(frame1, window, windowColor);
            draw(frame1, expWindow, expColor);
            imwrite(DATA_PATH + capName + "OUT/frame" + to_string(1) + ".jpg", frame1);
        }

        //apre dile di output del test
        results128.open(DATA_PATH + capName + "OUT/results128.txt", ofstream::out | ofstream::trunc);
    }

    //legge primo frame dal video e crea la finestra del modello nel primo frame
    else
        window = setup(cap, frame, live, capName);
    
    //modello e dimensioni finestra frame 1
    Point2d ogSize(window.width, window.height);
    Histogram qHist;

    //crea modello
    if (histogramType == COLOR_HISTOGRAM)
        qHist = buildTargetPdfByBackground(frame, window, histogramType);
    else
        qHist = buildTargetPdf(frame, window, histogramType);

    //media pesata aggiornata con la prima posizione
    Wma wma(WMA_NUM_ITEMS);
    updateWma(window, wma);
    
    // per ogni frame del video
    while (true) {

        frameNum++;

        //carica nuovo frame e finestra attesa se test temple color
        if (test128){
            String frameString = "/img/";
            if ((int) frameNum - 1000 < 0)
                frameString += "0";
            if ((int) frameNum - 100 < 0)
                frameString += "0";
            if ((int) frameNum - 10 < 0)
                frameString += "0";
            frameString += to_string(frameNum);

            frame = imread(DATA_PATH + capName + frameString + ".jpg");
            window128(expPos, expWindow);
            
        }

        //carica nuovo frame da stream se video
        else
            cap >> frame;

        //fine video
        if (frame.empty())
            break;

        //esegue algoritmo per frame corrente
        bhattaC = findTarget(frame, window, ogSize, qHist, useAngle, &meanIt);

        //scrive su file di output iterazioni e coefficiente per frame corrente
        results << meanIt << " ";
        results << to_string(bhattaC) << endl;
        
        //disegna le finestre sul frame
        if (test128)
            draw(frame, expWindow, expColor);
        draw(frame, window, windowColor);

        //scrive su file di output errore tra finestra attesa e ottenuta
        if (test128){

            int xc = window.x + window.width/2;
            int yc = window.y + window.height/2;
            int exc = expWindow.x + expWindow.width/2;
            int eyc = expWindow.y + expWindow.height/2;

            double err = sqrt(pow(xc - exc, 2) + pow(yc - eyc, 2));
            results128 << err << " ";
            results128 << expWindow.width << " " << expWindow.height << endl;
                
        }
        
        //salva frame corrente
        if (save)
            imwrite(DATA_PATH + capName + "OUT/frame" + to_string(frameNum) + ".jpg", frame);
        
        //mostra frame corrente
        imshow("", frame);

        //posiziona finestra ricerca iniziale secondo la media per il prossimo frame
        if (useAvg){
            updateWma(window, wma);
            Point2d cAvg = avgWma(wma);
            smoothPosWma(window, cAvg);
        }
            

        //per poter uscire o stoppare durante esecuzione
        char key = waitKey(toWait);
        if (key == 113) // q
            break;
        if (key == 27) //esc
            save = !save;
        if (key == 32 && !live) // space
            waitKey(0);
    
    }

    //chiude file
    results.close();
    expPos.close();
    results128.close();

}


int main()  {

    VideoCapture cap;
    String capName;
    String ext;
    
    int mode;
    int test128;

    while (true) {

        while (true) {

            //sceglie video o test di input
            mode = -1;
            test128 = 0;

            cout << "Input : ";
            cin >> capName;

           
            int extIdx = capName.find_last_of(".");
            if (extIdx >= 0){
                ext = capName.substr(extIdx, capName.length() - 1);
                capName = capName.substr(0, extIdx);
            }
            

            if (capName.compare("quit") == 0)
                return 0;

            else if (capName.compare("live") == 0) {
                
                int device;

                cout << "IdCam : ";
                cin >> device;

                cap.open(device + CAP_ANY);
                if (!cap.isOpened())
                    continue;
                    
                mode = 0;
            }

            else if (capName.compare("test128") == 0){
                cout << "TestName : ";
                cin >> capName;

                ifstream expPos;
                expPos.open(DATA_PATH + capName + "/" + capName + "_gt.txt");
                while (!expPos.is_open()){
                    cout << "TestName : ";
                    cin >> capName;

                    if (capName.compare("quit") == 0)
                        break;
                    expPos.open(DATA_PATH + capName + "/" + capName + "_gt.txt");
                }
                
                if (expPos.is_open()){
                    test128 = 1;
                    expPos.close();
                }
                else
                    continue;

            }

            else {

                cap.open(DATA_PATH + capName + ext);
                if (!cap.isOpened())
                    continue;

            }

            break;
        }

        //sceglie modalità di funzionamento dell' algoritmo
        if (mode != 0){
                
            cout << "[1] Kb Color" << endl;
            cout << "[2] Kb Color | angle" << endl;
            cout << "[3] Kb Color | avg" << endl;
            cout << "[4] Kb Color | angle | avg" << endl;
                
            cout << "[5] Kb Grad" << endl;
            cout << "[6] Kb Grad  | angle" << endl;
            cout << "[7] Kb Grad  | avg" << endl;
            cout << "[8] Kb Grad  | angle | avg" << endl;
            
            cout << "[9] Quit" << endl;

            cout << "Mode : ";
            cin >> mode;

            if (cin.fail()){
                cin.clear();
                cap.release();
                mode = -1;
                continue;
            }
        }

        //esegue run secondo la modalità scelta
        switch (mode) {

            case 0 :
                run(cap, capName, COLOR_HISTOGRAM, 1, 1, 0, test128);
                break;
            case 1 :
                run(cap, capName, COLOR_HISTOGRAM, 0, 0, 0, test128);
                break;
            case 2 :
                run(cap, capName, COLOR_HISTOGRAM, 0, 1, 0, test128);
                break;
            case 3 :
                run(cap, capName, COLOR_HISTOGRAM, 0, 0, 1, test128);         
                break;
            case 4 :
                run(cap, capName, COLOR_HISTOGRAM, 0, 1, 1, test128);
                break;
            case 5 :
                run(cap, capName, GRACL_HISTOGRAM, 0, 0, 0, test128);
                break;
            case 6 :
                run(cap, capName, GRACL_HISTOGRAM, 0, 1, 0, test128);
                break;
            case 7 :
                run(cap, capName, GRACL_HISTOGRAM, 0, 0, 1, test128);         
                break;
            case 8 :
                run(cap, capName, GRACL_HISTOGRAM, 0, 1, 1, test128);
                break;
            default :
                break;
            
        }

        cap.release();
        destroyAllWindows();
    }

    return 0;

}