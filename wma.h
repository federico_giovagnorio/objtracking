using namespace std;
using namespace cv;

#define WMA_NUM_ITEMS 20

class Wma {

public :

    int maxn;
    int n;
    list<Point2d> data;

    Wma();

    Wma(int max);

    void update(Point2d elem);

    Point2d avg();
    
};

void updateWma(Rect& window, Wma& wma);

cv::Point2d avgWma(Wma& wma);

void smoothPosWma(Rect& window, Point2d avg);
