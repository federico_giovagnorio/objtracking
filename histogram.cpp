#include "utils.h"
#include "histogram.h"

using namespace std;
using namespace cv;

//histogram.cpp : 7
Histogram::Histogram(){

    type = EMPTY_HISTOGRAM;

}

//histogram.cpp : 14
Histogram::Histogram(Mat frame, Rect window, int histogramType , int gnd){

    type = histogramType;

    bBins = BNBINS;
    gBins = GNBINS;
    rBins = GNBINS;
    
    buildData(frame, window, histogramType, gnd);

}

//histogram.cpp : 27
void Histogram::buildData(Mat frame, Rect window, int histogramType , int gnd){

    //azzera tutti gli elementi di data
    memset(data, 0, bBins * gBins * rBins * sizeof(float));

    //per istogrammi che utilizzano informazioni sul gradiente
    vector<Mat> mags;
    int iMag = 0;
    int jMag = 0;
    char valMag = 0;
    if (type == GRACL_HISTOGRAM)
        mags = getMagnitude(frame, window);

    //indici per scansionare la parte di frame relativa alla finestra
    int il = window.y;
    int ih = window.y + window.height;
    int jl = window.x;
    int jh = window.x + window.width;

    //simula ampiezza doppia della finestra se istogramma di sfondo
    if (gnd == BACKGROUND_HISTOGRAM){
        il -= window.height / 2;
        ih += window.height / 2;
        jl -= window.width / 2;
        jh += window.width / 2;
    }

    //coordinate centro finestra
    int xC = window.x + window.width/2;
    int yC = window.y + window.height/2;
                
    
    for (int i = il; i < ih; i++){
        
        //i pixel esterni al frame non contribuiscono ai valori dell'istogramma
        if (i < 0 || i >= frame.rows)
            continue;

        //distanza verticale del pixel dal centro, poi normalizzata 
        int relY = yC - i;
        double kerY = (double) relY / (window.height/2);

        for (int j = jl; j < jh; j++){

            //i pixel esterni al frame non contribuiscono ai valori dell'istogramma
            if (j < 0 || j >= frame.cols)
                continue;

            //distanza orizzontale del pixel dal centro, poi normalizzata
            int relX = xC - j;
            double kerX = (double) relX / (window.width / 2);
            
            //valore per il kernel, norma al quadrato della distanza normalizzata
            float valueForKernel = pow(kerY, 2) + pow(kerX, 2);

            //valore restituito dal kernel, è il contributo del pixel nell' istogramma
            float valueToAdd;
            if (gnd == FOREGROUND_HISTOGRAM) 
                valueToAdd = foregroundKernel(valueForKernel);
            else if (gnd == BACKGROUND_HISTOGRAM)
                valueToAdd = backgroundKernel(valueForKernel);
            
            //valori del pixel per ogni canale
            int bVal = frame.at<Vec3b>(i,j)[0];
            int gVal = frame.at<Vec3b>(i,j)[1];
            int rVal = frame.at<Vec3b>(i,j)[2];

            //per istogrammi che utilizzano informazioni sul gradiente
            if (type == GRACL_HISTOGRAM){
                
                char bMag = mags[0].at<Vec3b>(iMag, jMag)[0];
                char gMag = mags[1].at<Vec3b>(iMag, jMag)[0];
                char rMag = mags[2].at<Vec3b>(iMag, jMag)[0];
                char grayMag = mags[3].at<Vec3b>(iMag, jMag)[0];

                if (grayMag > MAG_TRESHOLD){

                    bVal -= bMag;
                    gVal -= gMag;
                    rVal -= rMag;
                
                }
                else {

                    bVal += bMag;
                    gVal += gMag;
                    rVal += rMag;
                
                }              

                bVal = max(bVal, 0);
                bVal = min(bVal, 255);   
                gVal = max(gVal, 0);
                gVal = min(gVal, 255);           
                rVal = max(rVal, 0);
                rVal = min(rVal, 255);

            }          
                                  
            //trova gli indici dell' istogramma relativi al pixel in esame
            int bIdx = bVal * ((float) bBins / 256);
            int gIdx = gVal * ((float) gBins / 256);
            int rIdx = rVal * ((float) rBins / 256);

            //aumenta la componente relativa al pixel dell' istogramma
            data[bIdx][gIdx][rIdx] += valueToAdd;

            jMag++;

        }

        iMag ++;       
    }

}

//histogram.cpp : 144
Histogram::~Histogram(){

}

//histogram.cpp : 149
bool Histogram::empty(){

    return type == EMPTY_HISTOGRAM;

}

//histogram.cpp : 156
void Histogram::normalize(){
    
    float sum = 0;
    
    for (int i = 0; i < bBins; i++)
        for (int j = 0; j < gBins; j++)
            for(int k = 0; k < rBins; k++)
                sum += data[i][j][k];

    for (int i = 0; i < bBins; i++)
        for (int j = 0; j < gBins; j++)
            for(int k = 0; k < rBins; k++)
                data[i][j][k] = (float) data[i][j][k] / sum;

}

//histogram.cpp : 173
float Histogram::minNonZero(){
    
    float min = 0;
    
    for (int i = 0; i < bBins; i++){
        for (int j = 0; j < gBins; j++){
            for(int k = 0; k < rBins; k++){
            
                float val = data[i][j][k];
                if (min == 0)
                    min = val;
                else if (val != 0 && val < min)
                    min = val;
            } 
        }
    }

    return min;

}

//histogram.cpp : 195
void Histogram::print(){

    cout << "----------------" << endl;
    cout << "TYPE : " << type << endl;
    cout << endl;

    cout << "BBINS : " << bBins << endl;
    cout << "GBINS : " << gBins << endl;
    cout << "RBINS : " << rBins << endl;
    cout << endl;

    for (int i = 0; i < bBins; i++)
        for (int j = 0; j < gBins; j++)
            for(int k = 0; k < rBins; k++)
                cout << "Bin: (" << i << "," << j << "," << k << ") Val:" << to_string(data[i][j][k]) << endl;
    
    cout << "----------------" << endl;

}

//histogram.cpp : 216
float foregroundKernel(float valueForKernel){

    float ret = 0;

    if (valueForKernel < 1)
        ret = 1 - valueForKernel;
        
    return ret;

}

//histogram.cpp : 228
float backgroundKernel(float valueForKernel){

    float ret = 0;

    if (valueForKernel > 1 && valueForKernel <= 2)
        ret = valueForKernel - 1;

    return ret;

}

//histogram.cpp : 240
vector<Mat> getMagnitude(Mat frame, Rect window){

    Rect inWindow = Rect(window);
    if (inWindow.x < 0)
        inWindow.x = 0;
    if (inWindow.y < 0)
        inWindow.y = 0;
    if (inWindow.x + inWindow.width  > frame.cols)
        inWindow.width = frame.cols - inWindow.x;
    if (inWindow.y + inWindow.height > frame.rows)
        inWindow.height = frame.rows - inWindow.y;
    
    Mat inImage = frame(inWindow);

    Mat channels[3];
    split(inImage, channels);
    Mat bChannel = channels[0];
    Mat gChannel = channels[1];
    Mat rChannel = channels[2];
    Mat gray;
    cvtColor(inImage, gray, COLOR_BGR2GRAY);
    
    Mat bBlur;
    Mat gBlur;
    Mat rBlur;
    Mat grayBlur;
    GaussianBlur(bChannel, bBlur, Size(3,3), 0, 0, BORDER_DEFAULT);
    GaussianBlur(gChannel, gBlur, Size(3,3), 0, 0, BORDER_DEFAULT);
    GaussianBlur(rChannel, rBlur, Size(3,3), 0, 0, BORDER_DEFAULT);
    GaussianBlur(gray,  grayBlur, Size(3,3), 0, 0, BORDER_DEFAULT);

    Mat bx, by;
    Mat gx, gy;
    Mat rx, ry;
    Mat grayx, grayy;
    Sobel(bBlur, bx, CV_32F, 1, 0, 3);
    Sobel(bBlur, by, CV_32F, 0, 1, 3);
    Sobel(gBlur, gx, CV_32F, 1, 0, 3);
    Sobel(gBlur, gy, CV_32F, 0, 1, 3);
    Sobel(rBlur, rx, CV_32F, 1, 0, 3);
    Sobel(rBlur, ry, CV_32F, 0, 1, 3);
    Sobel(grayBlur, grayx, CV_32F, 1, 0, 3);
    Sobel(grayBlur, grayy, CV_32F, 0, 1, 3);

    Mat bMag, gMag, rMag, grayMag;
    magnitude(bx, by, bMag);
    magnitude(gx, gy, gMag);
    magnitude(rx, ry, rMag);
    magnitude(grayx, grayy, grayMag);

    Mat bAbs, gAbs, rAbs, grayAbs;
    convertScaleAbs(bMag, bAbs);
    convertScaleAbs(gMag, gAbs);
    convertScaleAbs(rMag, rAbs);
    convertScaleAbs(grayMag, grayAbs);
    
    vector<Mat> mags;
    mags.push_back(bAbs);
    mags.push_back(gAbs);
    mags.push_back(rAbs);
    mags.push_back(grayAbs);

    return mags;

}

//histogram.cpp : 307
float bhattaCoeff(Histogram& hist1, Histogram& hist2){

    float ret = 0;

    for (int i = 0; i < hist1.bBins; i++)
        for (int j = 0; j < hist1.gBins; j++)
            for(int k = 0; k < hist1.rBins; k++)
                ret += sqrtf(hist1.data[i][j][k] * hist2.data[i][j][k]);
    
    return ret;

}

//histogram.cpp : 321
void histByBackground(Histogram& foreHist, Histogram& backHist){
    
    //fattore moltiplicativo componente u-sima
    float vu;

    //normalizza istogramma dello sfondo, calcola valore minimo
    backHist.normalize();
    float minBackVal = backHist.minNonZero();
    
    if (minBackVal == 0)
        return;
    
    //per ogni componente dell' istogramma
    for (int i = 0; i < foreHist.bBins; i++){
        for (int j = 0; j < foreHist.gBins; j++){
            for(int k = 0; k < foreHist.rBins; k++){
            
                //calcola rispettivo vu
                float backVal = backHist.data[i][j][k]; 
                if (backVal == 0)
                    vu = 1;
                else {    
                    vu = minBackVal / backVal;
                    if (vu > 1)
                        vu = 1;
                }

                //moltiplica componente u-sima del target per vu
                foreHist.data[i][j][k] *= vu;

            }
        }
    }

}

//histogram.cpp : 358                               
Histogram buildTargetPdf(Mat& frame, Rect& window, int histogramType){
    
    //costruisce istogramma del target e lo normalizza
    Histogram targetHist(frame, window,histogramType, FOREGROUND_HISTOGRAM);
    targetHist.normalize();

    return targetHist;

}


//histogram.cpp : 370 
Histogram buildTargetPdfByBackground(Mat& frame, Rect& window, int histogramType){
    
    //costruisce istogramma del target e del suo sfondo
    Histogram targetHist(frame, window, histogramType, FOREGROUND_HISTOGRAM);
    Histogram targetHistBackground(frame, window, histogramType, BACKGROUND_HISTOGRAM);

    //trasforma istogramma del target secondo informazioni dello sfondo, poi normalizza
    histByBackground(targetHist, targetHistBackground);
    targetHist.normalize();
    
    return targetHist;

}