#include <stdio.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <bits/stdc++.h>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define DATA_PATH "../data/"
#define TIME_TO_WAIT 20


Rect setup(VideoCapture& cap, Mat& frameToRet, int live, String capName);

void window128(ifstream& expPos, Rect& expWindow);

void drawPoint(Mat& frame, Point2d c, Scalar color);

void draw(Mat &frame, Rect window, Scalar rectColor);
