#include "utils.h"
#include "wma.h"

Wma::Wma(){
    
}

Wma::Wma(int max){
    n = 0;
    maxn = max;
}

void Wma::update(Point2d elem){
    
    if (data.size() >= maxn){
        data.pop_front();
        n--;
    }
    
    data.push_back(elem);
    n++;

}

Point2d Wma::avg(){

    long double xAvg = 0;
    long double yAvg = 0;


    int i = 1;
    for (list<Point2d>::iterator it = data.begin(); it != data.end(); it ++) {

        Point2d point = *it;

        xAvg += (double) point.x *  i  / ( n * (n + 1) / 2);
        yAvg += (double) point.y *  i  / ( n * (n + 1) / 2);
        
        i++;

    }

    unsigned int x = round(xAvg);
    unsigned int y = round(yAvg);

    return Point2d(x,y);
    
}

void updateWma(Rect& window, Wma& wma){

    int xc = window.x + window.width/2;
    int yc = window.y + window.height/2;

    wma.update(Point2d(xc, yc));

}

Point2d avgWma(Wma& wma){

    return wma.avg();

}

void smoothPosWma(Rect& window, Point2d avg){

    int xc = window.x + window.width/2;
    int yc = window.y + window.height/2;

    int nxc = avg.x;
    int nyc = avg.y;

    window.x += nxc - xc;
    window.y += nyc - yc;

}