#include "utils.h"


//seleziona zona di interesse per costruzione modello
Rect setup(VideoCapture& cap, Mat& frameToRet, int live, String capName){
    
    Mat frame;
    Rect window;

    char prec;
    char key;

    ofstream saveOut;
    ifstream saveIn;

    cap >> frameToRet;
    
    if(frameToRet.empty()){
        cout << "[!] Errore nell'apertura del frame 1" << endl;
        return Rect();
    }

    frame = frameToRet.clone();
    
    
    if (live) {

        window.width = frame.rows / 6;
        window.height = frame.cols / 6;
        window.x = frame.cols/2 - window.width/2;
        window.y = frame.rows/2 - window.height/2;

    }

    else {
        
        saveIn.open(DATA_PATH + capName + ".txt");
        if (saveIn.is_open()) {
        
            saveIn >> window.x;
            saveIn >> window.y;
            saveIn >> window.width;
            saveIn >> window.height;
            saveIn.close();
        
        }
        
        else {
            
            window = selectROI(frame);
            destroyAllWindows();
        
        }
        
    }
        
    bool toRet = false;
    
    while(!toRet){
        
        if (live) {

            cap >> frameToRet;
            
            if(frameToRet.empty()){
                cout << "[!] Errore nell'apertura del frame 1" << endl;
                return Rect();
            }
        
        }
        
        frame = frameToRet.clone();

        rectangle(frame, window, Scalar(255,255, 255), 1);
        imshow("", frame);
        
        key = waitKey(5);
        char prec = 0;

        if (live)
            prec += 4;

        switch (key) {
            case 119: //w
                window.y -= 2 + prec;
                break;
            case 115: //s
                window.y += 2 + prec;
                break;
            case 97:  //a
                window.x -= 2 + prec;
                break;
            case 100: //d
                window.x += 2 + prec;
                break;
            case 103: //g
                window.height += 2 + prec;
                break;
            case 116: //t
                window.height -= 2 + prec;
                break;
            case 102: //f
                window.width -= 2 + prec;
                break;
            case 104:  //h
                window.width += 2 + prec;
                break;
            case 114 : //r
                if (!live) {
                    destroyAllWindows();
                    window = selectROI(frameToRet);
                    destroyAllWindows();
                }
                break;
            case 13:  //enter  
                toRet = true;
                break;
            case 27: //esc
                if (!live){
                    saveOut.open(DATA_PATH + capName + ".txt", ofstream::out | ofstream::trunc);
                    if (saveOut.is_open()){
                        saveOut << window.x;
                        saveOut << " " << window.y;
                        saveOut << " " << window.width;
                        saveOut << " " << window.height << endl;
                    }
                    saveOut.close();
                }
                toRet = true;
                break;
            default:
                break;
            }
        } 

    window.height -= (window.height + 1) %2;
    window.width -=  (window.width + 1) %2;

    return window;

}

//aggiorna finestra attesa di un test per un frame
void window128(ifstream& expPos, Rect& expWindow){
    String expString;
    
    getline(expPos, expString, ',');
    if (expPos.eof())
        return;
    expWindow.x = stoi(expString);
    getline(expPos, expString, ',');
    expWindow.y = stoi(expString);
    getline(expPos, expString, ',');
    expWindow.width = stoi(expString);
    getline(expPos, expString);
    expWindow.height = stoi(expString);

}

//disegna punto (per centro media mobile o finestra)
void drawPoint(Mat& frame, Point2d c, Scalar color){

    circle(frame, c, 3, color, 2);

}

//disegna rettangolo finestra sul frame
void draw(Mat &frame, Rect window, Scalar rectColor){

    int xC = window.x + window.width/2;
    int yC = window.y + window.height/2;
    rectangle(frame, window, rectColor, 3);
    drawPoint(frame, Point2d(xC, yC), rectColor);
    
}