#include "utils.h"
#include "histogram.h"
#include "tracking.h"

//tracking.cpp : 5
Mat createWImage(Mat& frame, Rect& window, Histogram& qHist, Histogram& pHist){
    
    //immagine delle stesse dimensioni di un target
    Mat wImage(window.height, window.width, CV_32FC1);

    //scansiona la parte di frame relativa alla finestra
    for (int i = 0; i < wImage.rows; i++){
        int yCoord = i + window.y;

        for (int j = 0; j < wImage.cols; j++){
            int xCoord = j + window.x;

            //valori nulli per punti della finestra esterni al frame
            if (xCoord < 0 || xCoord >= frame.cols || yCoord < 0 || yCoord >= frame.rows)
                wImage.at<float>(i,j) = 0;
            //calcola wi per ogni pixel
            else {
                //indici istogramma per pixel corrente
                int bIdx = frame.at<Vec3b>(yCoord, xCoord)[0] * ((float) qHist.bBins / 256);
                int gIdx = frame.at<Vec3b>(yCoord, xCoord)[1] * ((float) qHist.gBins / 256);
                int rIdx = frame.at<Vec3b>(yCoord, xCoord)[2] * ((float) qHist.rBins / 256);
                //valori istogramma di q e p per pixel corrente
                float qVal = qHist.data[bIdx][gIdx][rIdx];
                float pVal = pHist.data[bIdx][gIdx][rIdx];

                //wi
                float value = 0;
                if (pVal != 0)
                    value = sqrt(qVal / pVal );
                //rende valore del pixel al relativo wi
                wImage.at<float>(i,j) = value;

            }

        }
    }
    
    return wImage;
}

//tracking.cpp : 46
Point2d findCentroid(Mat& wImage, int x, int y){

    //momenti dell' immagine di un target
    long double m00 = 0.0;
    long double m10 = 0.0;
    long double m01 = 0.0;

    //scansiona immagine del target
    for(int i = 0; i < wImage.rows; i++){

        int yCoord = y + i;
            
        for(int j = 0; j < wImage.cols; j++){
    
            int xCoord  = x + j;               
            
            //aggiorna momenti
            m00 += wImage.at<float>(i, j);
            m01 += wImage.at<float>(i, j) * yCoord;
            m10 += wImage.at<float>(i, j) * xCoord;
        
        }
    }

    //caso particolare m00 nullo
    if (m00 == 0)
        return Point2d(-1,-1);

    //coordinate centroide
    int newXC = round(m10/m00);
    int newYC = round(m01/m00);
    
    return Point2d(newXC, newYC);

}

//tracking.cpp : 83
float findLocation(Mat& frame, Rect& window, Mat& wImage, Histogram& qHist, 
                                                        unsigned int* meanIt){ 
                
    Histogram pHist;
    Point2d newCenter;
    int iter = 0;

    //sposta finestra finche non si raggiunge numero massimo iterazioni MeanShift
    while(iter < 20) {

        iter++;

        //coordinate centro finestra
        int yC = window.y + window.height/2;
        int xC = window.x + window.width/2;

        //crea istogramma del target relativo alla finestra corrente
        pHist = buildTargetPdf(frame, window, qHist.type);

        //crea immagine del target corrente
        wImage = createWImage(frame, window, qHist, pHist);

        //calcola centroide dell' immagine del target
        newCenter = findCentroid(wImage, window.x, window.y);
        //caso particolare m00 nullo
        if (newCenter.x == -1 || newCenter.y == -1)
            return -1;

        //calcola distanza tra centro e centroide, cioè lo spostamento
        int dx = newCenter.x - xC;
        int dy = newCenter.y - yC;

        //centra la finestra nel centroide
        window.y = newCenter.y - window.height/2;
        window.x = newCenter.x - window.width/2;

        //se spostamento minore di epsilon termina
        if (sqrt(pow(dx,2) + pow(dy,2)) < 2)
            break;

    }

    *meanIt = iter;
    float bhattaC = bhattaCoeff(qHist, pHist);

    return bhattaC;
}

//tracking.cpp : 132
double findAngle(Mat& frame, Mat& wImage, Rect& window){

    //momenti dell' immagine di un target
    long double m00 = 0.0;
    long double m02 = 0.0;
    long double m20 = 0.0;
    long double m11 = 0.0;

    //scansiona immagine del target
    for (int i = 0; i < wImage.rows; i++){
        
        int yCoord = i - wImage.rows/2 ;

        for (int j = 0; j < wImage.cols; j++){
            
            int xCoord = j - wImage.cols/2;

            //aggiorna momenti
            m00 += wImage.at<float>(i, j);
            m02 += wImage.at<float>(i, j) * pow(yCoord,2);
            m20 += wImage.at<float>(i, j) * pow(xCoord,2);
            m11 += wImage.at<float>(i, j) * yCoord * xCoord;

        }
    }

    //valori matrice covarianza
    long double u20 = m20 / m00;
    long double u02 = m02 / m00;
    long double u11 = m11 / m00;

    double val =  2 * u11 / (u20 - u02);
    
    //angolo direzione asse maggiore, varia circolarmente tra 0 e 180 gradi 
    double theta = atan( val ) / 2 + CV_PI/2;
    if (u11 > 0 && u20 > u02)
        theta -= CV_PI/2;
    if (u11 < 0 && u20 > u02)
        theta  += CV_PI/2;

    
    return theta;
}

//tracking.cpp : 177
float findTarget(Mat& frame, Rect& window, Point2d& ogSize, Histogram& qHist, 
                                            int useAngle, unsigned int* meanIt){
    
    //immagine del target di convergenza
    Mat wImage;
    //trova target più simile nel frame corrente, aggiorna wImage
    float bhattaC = findLocation(frame, window, wImage, qHist, meanIt);
    
    //se si utilizza la funzionalità dell' angolo
    if (useAngle){

        //trova direzione somiglianza
        double theta = findAngle(frame, wImage, window);

        //b massimo tra altezza e larghezza del target modello, a minimo
        int b = max(ogSize.x, ogSize.y) / 2; 
        int a = min(ogSize.x, ogSize.y) / 2;
        int xC = window.x + window.width/2;
        int yC = window.y + window.height/2;

        //componenti verticali b secondo theta
        int y1 = round(yC - b*sin(theta));
        int y2 = round(yC + b*sin(theta)); 

        //aggiorna altezza e larghezza finestra nei limiti consentiti
        int nH = abs(y2 - y1);
        if (nH < a * 2)
            nH = a * 2;
        int nW = ogSize.x + ogSize.y - nH;
        window.x = xC - nW/2;
        window.y = yC - nH/2;
        window.width = nW;
        window.height = nH;
    }

    return bhattaC;

}