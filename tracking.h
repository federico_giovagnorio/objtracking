using namespace std;
using namespace cv;

//crea immagine di somiglianza tra q e p
Mat createWImage(Mat& currFrame, Rect& window, Histogram& qHist, Histogram& pHist);

//calcola nuovo centro della finestra di ricerca ricerca
Point2d findCentroid(Mat& wImage, int x, int y);

//trova posizione migliore, aggiorna posizione finestra, e torna il BhattaC
float findLocation(Mat& frame, Rect& window, Mat& wImage, Histogram& qHist, unsigned int* meanSum);

//calcola angolo direzione asse maggiore della somiglianza in una finestra
double findAngle(Mat& frame, Mat& wImage, Rect& window);

//trova posizione e forma della finestra
float findTarget(Mat& frame, Rect& window, Point2d& ogSize, Histogram& qHist, int useAngle, unsigned int* meanSum);